// const CONFIG = {};

// CONFIG.apiBaseUrl =
//   "http://admin-snack.13.251.251.232.nip.io";

// CONFIG.documentFetchurl = "http://upload-snack.13.251.251.232.nip.io"
// CONFIG.verifyUrl = "http://admin-snack.13.251.251.232.nip.io";
// CONFIG.token = localStorage.getItem('token');
// module.exports = CONFIG;

const CONFIG = {};

let apiBaseUrl, documentFetchUrl, verifyUrl;
const hostname = window && window.location && window.location.hostname;

if (hostname === "admin.sit.snackbyincome.sg") {
  apiBaseUrl = "https://zip-apigw.sit.income.com.sg/snack/admin";
  documentFetchUrl = "https://zip-apigw.sit.income.com.sg/snack/upload";
  verifyUrl = "https://zip-apigw.sit.income.com.sg/snack/admin";
} else if (hostname === "admin.uat.snackbyincome.sg") {
  apiBaseUrl = "https://zip-apigw.uat.income.com.sg/snack/admin";
  documentFetchUrl = "https://zip-apigw.uat.income.com.sg/snack/upload";
  verifyUrl = "https://zip-apigw.uat.income.com.sg/snack/admin";
} else if (hostname === "admin.snackbyincome.sg") {
  apiBaseUrl = "https://apigw.digitalincome.com.sg/snack/admin";
  documentFetchUrl = "https://apigw.digitalincome.com.sg/snack/upload";
  verifyUrl = "https://apigw.digitalincome.com.sg/snack/admin";
} else if (hostname === "admin.dev.snackbyincome.sg") {
  apiBaseUrl = "https://api.dev.snackbyincome.sg/admin";
  documentFetchUrl = "https://api.dev.snackbyincome.sg/upload";
  verifyUrl = "https://api.dev.snackbyincome.sg/admin";
} else {
  apiBaseUrl =
    process.env.REACT_APP_API_BASE_URL ||
    "https://zip-apigw.sit.income.com.sg/snack/admin";
  documentFetchUrl =
    process.env.REACT_APP_DOCUMENT_FETCH_URL ||
    "https://zip-apigw.sit.income.com.sg/snack/upload";
  verifyUrl =
    process.env.REACT_APP_VERIFY_URL ||
    "https://zip-apigw.sit.income.com.sg/snack/admin";
}

if (process.env.REACT_APP_ENV === "DEV") {
  const API_HOST = "https://api.dev.snackbyincome.sg/";

  apiBaseUrl = API_HOST + "admin";
  documentFetchUrl = API_HOST + "upload";
  verifyUrl = API_HOST + "admin";
}

CONFIG.apiBaseUrl = apiBaseUrl;
CONFIG.documentFetchUrl = documentFetchUrl;
CONFIG.verifyUrl = verifyUrl;
CONFIG.token = localStorage.getItem("token");

module.exports = CONFIG;
