export const GET_USERS_STARTED = 'GET_USERS_STARTED';
export const GET_USERS_SUCCEEDED = 'GET_USERS_SUCCEEDED';
export const GET_USERS_FAILED = 'GET_USERS_FAILED';

export const GET_PRODUCT_STARTED = 'GET_PRODUCT_STARTED';
export const GET_PRODUCT_SUCCEEDED = 'GET_PRODUCT_SUCCEEDED';
export const GET_PRODUCT_FAILED = 'GET_PRODUCT_FAILED';

export const UPDATE_PRODUCT_STARTED = 'UPDATE_PRODUCT_STARTED';
export const UPDATE_PRODUCT_SUCCEEDED = 'UPDATE_PRODUCT_SUCCEEDED';
export const UPDATE_PRODUCT_FAILED = 'UPDATE_PRODUCT_FAILED';

export const GET_DOCUMENT_STARTED = 'GET_DOCUMENT_STARTED';
export const GET_DOCUMENT_SUCCEEDED = 'GET_DOCUMENT_SUCCEEDED';
export const GET_DOCUMENT_FAILED = 'GET_DOCUMENT_FAILED';

export const VERIFY_DOCUMENT_STARTED = 'VERIFY_DOCUMENT_STARTED';
export const VERIFY_DOCUMENT_SUCCEEDED = 'VERIFY_DOCUMENT_SUCCEEDED';
export const VERIFY_DOCUMENT_FAILED = 'VERIFY_DOCUMENT_FAILED';

export const GET_TRIGGER_STARTED = 'GET_TRIGGER_STARTED';
export const GET_TRIGGER_SUCCEEDED = 'GET_TRIGGER_SUCCEEDED';
export const GET_TRIGGER_FAILED = 'GET_TRIGGER_FAILED';

export const UPDATE_TRIGGER_STARTED = 'UPDATE_TRIGGER_STARTED';
export const UPDATE_TRIGGER_SUCCEEDED = 'UPDATE_TRIGGER_SUCCEEDED';
export const UPDATE_TRIGGER_FAILED = 'UPDATE_TRIGGER_FAILED';

export const GET_TRIGGER_SOURCES_STARTED = 'GET_TRIGGER_SOURCES_STARTED';
export const GET_TRIGGER_SOURCES_SUCCEEDED = 'GET_TRIGGER_SOURCES_SUCCEEDED';
export const GET_TRIGGER_SOURCES_FAILED = 'GET_TRIGGER_SOURCES_FAILED';

export const FETCH_IMAGE_STARTED = 'FETCH_IMAGE_STARTED';
export const FETCH_IMAGE_SUCCEEDED = 'FETCH_IMAGE_SUCCEEDED';
export const FETCH_IMAGE_FAILED = 'FETCH_IMAGE_FAILED';

export const UPDATE_TRIGGER_SOURCE_STARTED = 'UPDATE_TRIGGER_SOURCE_STARTED';
export const UPDATE_TRIGGER_SOURCE_SUCCEEDED = 'UPDATE_TRIGGER_SOURCE_SUCCEEDED';
export const UPDATE_TRIGGER_SOURCE_FAILED = 'UPDATE_TRIGGER_SOURCE_FAILED';

export const GET_NOTIFICATIONS_STARTED = 'GET_NOTIFICATIONS_STARTED';
export const GET_NOTIFICATIONS_SUCCEEDED = 'GET_NOTIFICATIONS_SUCCEEDED';
export const GET_NOTIFICATIONS_FAILED = 'GET_NOTIFICATIONS_FAILED';

export const CREATE_NOTIFICATIONS_STARTED = 'CREATE_NOTIFICATIONS_STARTED';
export const CREATE_NOTIFICATIONS_SUCCEEDED = 'CREATE_NOTIFICATIONS_SUCCEEDED';
export const CREATE_NOTIFICATIONS_FAILED = 'CREATE_NOTIFICATIONS_FAILED';

export const UPDATE_NOTIFICATIONS_STARTED = 'UPDATE_NOTIFICATIONS_STARTED';
export const UPDATE_NOTIFICATIONS_SUCCEEDED = 'UPDATE_NOTIFICATIONS_SUCCEEDED';
export const UPDATE_NOTIFICATIONS_FAILED = 'UPDATE_NOTIFICATIONS_FAILED';

export const GET_FAQ_STARTED = 'GET_FAQ_STARTED';
export const GET_FAQ_SUCCEEDED = 'GET_FAQ_SUCCEEDED';
export const GET_FAQ_FAILED = 'GET_FAQ_FAILED';

export const GET_FAQ_CATEGORIES_STARTED = 'GET_FAQ_CATEGORIES_STARTED';
export const GET_FAQ_CATEGORIES_SUCCEEDED = 'GET_FAQ_CATEGORIES_SUCCEEDED';
export const GET_FAQ_CATEGORIES_FAILED = 'GET_FAQ_CATEGORIES_FAILED';

export const CREATE_FAQ_STARTED = 'CREATE_FAQ_STARTED';
export const CREATE_FAQ_SUCCEEDED = 'CREATE_FAQ_SUCCEEDED';
export const CREATE_FAQ_FAILED = 'CREATE_FAQ_FAILED';

export const UPDATE_FAQ_STARTED = 'UPDATE_FAQ_STARTED';
export const UPDATE_FAQ_SUCCEEDED = 'UPDATE_FAQ_SUCCEEDED';
export const UPDATE_FAQ_FAILED = 'UPDATE_FAQ_FAILED';

export const CHECK_AUTHORIZATION_STARTED = 'CHECK_AUTHORIZATION_STARTED';
export const CHECK_AUTHORIZATION_SUCCEEDED = 'CHECK_AUTHORIZATION_SUCCEEDED';
export const CHECK_AUTHORIZATION_FAILED = 'CHECK_AUTHORIZATION_FAILED';

export const GET_TANDC_STARTED = 'GET_TANDC_STARTED';
export const GET_TANDC_SUCCEEDED = 'GET_TANDC_SUCCEEDED';
export const GET_TANDC_FAILED = 'GET_TANDC_FAILED';

export const GET_SYSTEM_LOGS_STARTED = 'GET_SYSTEM_LOGS_STARTED';
export const GET_SYSTEM_LOGS_SUCCEEDED = 'GET_SYSTEM_LOGS_SUCCEEDED';
export const GET_SYSTEM_LOGS_FAILED = 'GET_SYSTEM_LOGS_FAILED';

export const UPDATE_GOODS_TITLE_STARTED = 'UPDATE_GOODS_TITLE_STARTED';
export const UPDATE_GOODS_TITLE_SUCCEEDED = 'UPDATE_GOODS_TITLE_SUCCEEDED';
export const UPDATE_GOODS_TITLE_FAILED = 'UPDATE_GOODS_TITLE_FAILED';