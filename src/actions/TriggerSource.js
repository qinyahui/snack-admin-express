import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';
const getTriggerSourcesStarted = data =>({
    type: types.GET_TRIGGER_SOURCES_STARTED,
    payload: data

});

const getTriggerSourcesSuccess = data =>({
    type: types.GET_TRIGGER_SOURCES_SUCCEEDED,
    payload: data
});

const getTriggerSourcesFailed = data =>({
    type: types.GET_TRIGGER_SOURCES_FAILED,
    payload: data
});

const updateTriggerSourceStarted = data =>({
  type: types.UPDATE_TRIGGER_SOURCE_STARTED,
  payload: data

});

const updateTriggerSourceSuccess = data =>({
  type: types.UPDATE_TRIGGER_SOURCE_SUCCEEDED,
  payload: data
});

const updateTriggerSourceFailed = data =>({
  type: types.UPDATE_TRIGGER_SOURCE_FAILED,
  payload: data
});

export const getTriggerSources = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getTriggerSourcesStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/admin/getTriggerSource?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`,{headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(getTriggerSourcesSuccess(res.data));
        })
        .catch(err => {
          dispatch(getTriggerSourcesFailed(err.message));
        });
    };
  };

  export const updateTriggerSource = (params, token) => {
    return dispatch => {
      dispatch(updateTriggerSourceStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/admin/updateTriggerSource`, params,{headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(updateTriggerSourceSuccess(res.data));
        })
        .catch(err => {
          dispatch(updateTriggerSourceFailed(err.message));
        });
    };
  };