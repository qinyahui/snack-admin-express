import * as types from "../lib/ActionTypes";
import CONFIG from "../lib/config";
import axios from "axios";
const getUsersStarted = data => ({
  type: types.GET_USERS_STARTED,
  payload: data
});

const getUsersSuccess = data => ({
  type: types.GET_USERS_SUCCEEDED,
  payload: data
});

const getUsersFailed = data => ({
  type: types.GET_USERS_FAILED,
  payload: data
});

const getDocumentStarted = data => ({
  type: types.GET_DOCUMENT_STARTED,
  payload: data
});

const getDocumentSuccess = data => ({
  type: types.GET_DOCUMENT_SUCCEEDED,
  payload: data
});

const getDocumentFailed = data => ({
  type: types.GET_DOCUMENT_FAILED,
  payload: data
});

const verifyDocumentsStarted = data => ({
  type: types.VERIFY_DOCUMENT_STARTED,
  payload: data
});

const verifyDocumentsSuccess = data => ({
  type: types.VERIFY_DOCUMENT_SUCCEEDED,
  payload: data
});

const verifyDocumentsFailed = data => ({
  type: types.VERIFY_DOCUMENT_FAILED,
  payload: data
});

export const getUsers = (page, limit, searchText, token) => {
  console.log(CONFIG.token);
  console.log(token);
  return dispatch => {
    dispatch(getUsersStarted());

    axios
      .get(
        `${CONFIG.apiBaseUrl}/users?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`,
        {
          headers: {
            authorization: token
          }
        }
      )
      .then(res => {
        dispatch(getUsersSuccess(res.data));
      })
      .catch(err => {
        dispatch(getUsersFailed(err.message));
      });
  };
};

export const fetchDocuments = (fileName, token) => {
  return dispatch => {
    dispatch(getDocumentStarted());

    axios
      .get(`${CONFIG.documentFetchUrl}/fetch/base64?fileName=${fileName}`, {
        headers: {
          authorization: CONFIG.token === null ? token : CONFIG.token
        }
      })
      .then(res => {
        dispatch(getDocumentSuccess(res.data));
      })
      .catch(err => {
        dispatch(getDocumentFailed(err.message));
      });
  };
};

export const verifyDocuments = (params, token) => {
  return dispatch => {
    dispatch(verifyDocumentsStarted());

    axios
      .post(`${CONFIG.verifyUrl}/verify-documents`, params, {
        headers: {
          authorization: token
        }
      })
      .then(res => {
        dispatch(verifyDocumentsSuccess(res.data));
      })
      .catch(err => {
        dispatch(verifyDocumentsFailed(err.message));
      });
  };
};
