import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const getTriggersStarted = data =>({
    type: types.GET_TRIGGER_STARTED,
    payload: data

});

const getTriggersSuccess = data =>({
    type: types.GET_TRIGGER_SUCCEEDED,
    payload: data
});

const getTriggersFailed = data =>({
    type: types.GET_TRIGGER_FAILED,
    payload: data
});

const updateTriggersStarted = data =>({
  type: types.UPDATE_TRIGGER_STARTED,
  payload: data

});

const updateTriggersSuccess = data =>({
  type: types.UPDATE_TRIGGER_SUCCEEDED,
  payload: data
});

const updateTriggersFailed = data =>({
  type: types.UPDATE_TRIGGER_FAILED,
  payload: data
});

export const getTriggers = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getTriggersStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/triggers?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`,{headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(getTriggersSuccess(res.data));
        })
        .catch(err => {
          dispatch(getTriggersFailed(err.message));
        });
    };
  };

  export const updateTriggers = (params, token) => {
    return dispatch => {
      dispatch(updateTriggersStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/triggers/update`, params,{headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(updateTriggersSuccess(res.data));
        })
        .catch(err => {
          dispatch(updateTriggersFailed(err.message));
        });
    };
  };