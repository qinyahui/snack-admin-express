import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const getSystemLogsStarted = data =>({
    type: types.GET_SYSTEM_LOGS_STARTED,
    payload: data

});

const getSystemLogsSuccess = data =>({
    type: types.GET_SYSTEM_LOGS_SUCCEEDED,
    payload: data
});

const getSystemLogsFailed = data =>({
    type: types.GET_SYSTEM_LOGS_FAILED,
    payload: data
});

export const getSystemLogs = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getSystemLogsStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/audit-logs?pagenumber=${page}&pagesize=${limit}&searhtext=${searchText}`,{headers:{
          authorization:  token
        }})
        .then(res => {
          dispatch(getSystemLogsSuccess(res.data));
        })
        .catch(err => {
          dispatch(getSystemLogsFailed(err.message));
        });
    };
  };