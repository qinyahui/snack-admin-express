import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const fetchImageStarted = data =>({
    type: types.FETCH_IMAGE_STARTED,
    payload: data

});

const fetchImageSuccess = data =>({
    type: types.FETCH_IMAGE_SUCCEEDED,
    payload: data
});

const fetchImageFailed = data =>({
    type: types.FETCH_IMAGE_FAILED,
    payload: data
});

const checkAuthorizationStarted = data =>({
  type: types.CHECK_AUTHORIZATION_STARTED,
  payload: data

});

const checkAuthorizationSuccess = data =>({
  type: types.CHECK_AUTHORIZATION_SUCCEEDED,
  payload: data
});

const checkAuthorizationFailed = data =>({
  type: types.CHECK_AUTHORIZATION_FAILED,
  payload: data
});

export const fetchImages = (fileName) => {
    return dispatch => {
      dispatch(fetchImageStarted());
  
      axios
        .get(`${CONFIG.documentFetchurl}/fetch?fileName=${fileName}`,{headers:{
          authorization: CONFIG.token
        }})
        .then(res => {
          dispatch(fetchImageSuccess(res.data));
        })
        .catch(err => {
          dispatch(fetchImageFailed(err.message));
        });
    };
  };

export const checkAuthorization = (params) =>{
  return dispatch => {
    dispatch(checkAuthorizationStarted());

    axios
      .get(`${CONFIG.apiBaseUrl}/authorize${params}`,{headers:{
        authorization: CONFIG.token
      }})
      .then(res => {
        dispatch(checkAuthorizationSuccess(res.data));
      })
      .catch(err => {
        dispatch(checkAuthorizationFailed(err.message));
      });
  };
}