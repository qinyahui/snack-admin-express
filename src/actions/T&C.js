import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const getTandCStarted = data =>({
    type: types.GET_TANDC_STARTED,
    payload: data

});

const getTandCSuccess = data =>({
    type: types.GET_TANDC_SUCCEEDED,
    payload: data
});

const getTandCFailed = data =>({
    type: types.GET_TRIGGER_FAILED,
    payload: data
});

export const getTandC = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getTandCStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/terms-and-conditions?pagenumber=${page}&pagesize=${limit}&searhtext=${searchText}`,{headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(getTandCSuccess(res.data));
        })
        .catch(err => {
          dispatch(getTandCFailed(err.message));
        });
    };
  };