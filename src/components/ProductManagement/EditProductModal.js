import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Input,
  Row,
  Col,
  Label
} from "reactstrap";
import uploadToS3 from "../../apis/uploadToS3";

export default class EditProductModal extends Component {
  constructor() {
    super();

    this.state = {
      active: "",
      description: "",
      goodsName: "",
      flag: false,
      fileUrl: "",
      id: 1,
      goodsTitleUrls: []
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (!state.flag && props.item !== undefined) {
      return {
        description: props.item.description,
        goodsName: props.item.goodsName,
        active: props.item.status === 1 ? "Active" : "Inactive",
        id: props.item.id,
        fileUrl: `${props.item.emptyMascot}`,
        goodsTitleUrls: props.item.goodsTitle.map(item => {
          return `${item.imageUrl}`;
        })
      };
    } else {
      return {
        description: state.description,
        goodsName: state.goodsName,
        active: state.active,
        flag: false,
        fileUrl: state.fileUrl
      };
    }
  }

  changeInputText = e => {
    const name = e.target.name;
    this.setState({
      [name]: e.target.value,
      flag: true
    });
  };

  toggle = () => {
    this.props.togglePopup();
  };

  updateProduct = () => {
    const data = {
      id: this.state.id,
      description: this.state.description,
      goodsName: this.state.goodsName
    };
    this.props.updateProduct(data);
  };

  check = e => {
    this.setState({
      active: e.target.value,
      flag: true
    });
  };

  fileUpload = () => {
    document.getElementById("fileLoader").click();
  };

  onImageChange = (key, e, fileName, i, obj) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    let data = { id: this.state.id };

    reader.onloadend = () => {
      if (i !== undefined && i !== "photo") {
        let { goodsTitleUrls } = this.state;
        goodsTitleUrls[i] = reader.result;
        this.setState({
          goodsTitleUrls,
          flag: true
        });
      } else if (i === "photo") {
        this.setState({
          fileUrl: reader.result,
          flag: true
        });
      }
    };

    reader.readAsDataURL(file);

    uploadToS3(file).then(res => {
      if (key === "emptyMascot") {
        data.emptyMascot = res;
      } else if (key === "goodsTitle") {
        data = { id: obj.id, titleCode: obj.titleCode, imageUrl: res };
      }

      if (key === "goodsTitle") {
        this.props.updateProductTitle(data);
      } else {
        this.props.updateProduct(data);
      }
    });
  };

  onFileChange = (key, e, itemIndex) => {
    const file = e.target.files[0];
    const data = {
      id: this.state.id
    };

    uploadToS3(file).then(res => {
      if (key === "portfolioJson") {
        data.portfolioJson = `["${res}"]`;
      } else if (key === "mascotJson") {
        let string = "";

        this.props.item.mascotJson.map(
          (item, index) =>
            (string += `"${index === itemIndex ? res : item}"${
              index === this.props.item.mascotJson.length - 1 ? "" : ","
            }`)
        );

        data.mascotJson = `[${string}]`;
      }

      this.props.updateProduct(data);
    });
  };

  render() {
    const { item } = this.props;
    let { fileUrl } = this.state;

    let { mascotJson, portfolioJson } = item;

    // let goodsTitleImages = item.goodsTitle.map((file) => {
    //     return file;
    // })
    return (
      <div>
        <Modal
          isOpen={this.props.isShow}
          toggle={() => this.toggle()}
          className={this.props.className}
        >
          <ModalHeader toggle={() => this.toggle()}>Edit Product</ModalHeader>
          <ModalBody>
            {item !== undefined ? (
              <Container>
                <Row>
                  <Col xs="3">
                    <Label>Status</Label>
                  </Col>
                  <Col xs="auto">{this.state.active}</Col>
                </Row>
                <Row>
                  <Col xs="3">
                    <Label>Product Name</Label>
                  </Col>
                  <Col xs="auto">
                    <Input
                      onChange={e => this.changeInputText(e)}
                      name="goodsName"
                      value={this.state.goodsName}
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="3">
                    <Label>Description</Label>
                  </Col>
                  <Col xs="auto">
                    <Input
                      onChange={e => this.changeInputText(e)}
                      name="description"
                      type="textarea"
                      style={{ height: "100px" }}
                      value={this.state.description}
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="3">Mascot Json</Col>
                  <Col style={{ paddingLeft: "28px" }}>
                    {mascotJson &&
                      mascotJson.map((file, index) => (
                        <Row key={index}>
                          <div style={{ paddingLeft: "3px" }}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={file}
                            >
                              {file.substring(file.lastIndexOf("/") + 1)}
                            </a>
                            <Button
                              color="success"
                              className="replace-btn"
                              type="file"
                              onClick={() =>
                                document
                                  .getElementById(`fileLoader_${index}`)
                                  .click()
                              }
                            >
                              Replace
                            </Button>
                            <input
                              type="file"
                              id={`fileLoader_${index}`}
                              name="files"
                              title="Load File"
                              style={{ display: "none" }}
                              onChange={e => {
                                this.onFileChange("mascotJson", e, index);
                              }}
                            />
                          </div>
                        </Row>
                      ))}
                  </Col>
                </Row>
                <Row>
                  <Col xs="3">PortFolio Json</Col>
                  <Col style={{ paddingLeft: "28px" }}>
                    {portfolioJson &&
                      portfolioJson.map((file, index) => (
                        <Row key={index}>
                          <div style={{ paddingLeft: "3px" }}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={file}
                            >
                              {file.substring(file.lastIndexOf("/") + 1)}
                            </a>
                            <Button
                              color="success"
                              className="replace-btn"
                              type="file"
                              onClick={() =>
                                document
                                  .getElementById(`fileLoader1_${index}`)
                                  .click()
                              }
                            >
                              Replace
                            </Button>
                            <input
                              type="file"
                              id={`fileLoader1_${index}`}
                              name="files"
                              title="Load File"
                              style={{ display: "none" }}
                              onChange={e => {
                                this.onFileChange("portfolioJson", e);
                              }}
                            />
                          </div>
                        </Row>
                      ))}
                  </Col>
                </Row>
                <Row>
                  <Col xs="3">Empty Mascot</Col>
                  <Col>
                    <img
                      src={fileUrl}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document.getElementById("portfolio_img").click()
                      }
                      alt=""
                    />
                    <Input
                      id="portfolio_img"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.onImageChange(
                          "emptyMascot",
                          e,
                          item.emptyMascot.slice(60, item.emptyMascot.length),
                          "photo"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="3">Goods Title</Col>
                  <Col style={{ display: "flex" }}>
                    {item.goodsTitle.map((file, index) => (
                      <div style={{ marginRight: "12px" }} key={index}>
                        <img
                          src={this.state.goodsTitleUrls[`${index}`]}
                          alt=""
                          type="file"
                          className="portfoilio-img"
                          onClick={() =>
                            document
                              .getElementById(`portfoilio_img_${index}`)
                              .click()
                          }
                        ></img>
                        <Input
                          id={`portfoilio_img_${index}`}
                          type="file"
                          style={{ display: "none" }}
                          onChange={e =>
                            this.onImageChange(
                              "goodsTitle",
                              e,
                              file.imageUrl.slice(60, file.imageUrl.length),
                              index,
                              file
                            )
                          }
                        ></Input>
                      </div>
                    ))}
                  </Col>
                </Row>
              </Container>
            ) : (
              ""
            )}
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={() => this.updateProduct()}>
              Update
            </Button>
            <Button color="secondary" onClick={() => this.toggle()}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
