import React, { Component } from "react";
import { Card, CardHeader, Button, Table } from "reactstrap";
import EditProductModal from "./EditProductModal";
import Loader from "../Loader/Loader";
import UnAuthorized from "../../UnAuthorized/UnAuthorized";

export default class ProductList extends Component {
  constructor() {
    super();
    this.loading = true;
    this.state = {
      isOpen: false,
      activePage: 1,
      rowData: {}
    };
  }
  componentDidMount = () => {
    this.props.getProducts(this.state.activePage, 10, "", this.props.token);
    // this.props.fetchImages("1578468544595.png");
    this.loading = false;
  };
  togglePopup = row => {
    this.setState({
      isOpen: !this.state.isOpen,
      rowData: row
    });
  };
  updateProductTitle = async data => {
    this.loading = true;
    await this.props.updateGoodsTitle(data, this.props.token);
    setTimeout(() => {
      this.props.getProducts(this.state.activePage, 10, "", this.props.token);
      this.loading = false;
    }, 2000);
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  updateProduct = async data => {
    this.loading = true;
    await this.props.updateProducts(data);
    setTimeout(() => {
      this.props.getProducts(this.state.activePage, 10, "", this.props.token);
      this.loading = false;
    }, 2000);
    this.setState({
      isOpen: !this.state.isOpen
    });
  };
  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
  }
  render() {
    const columns = ["Name", "Description", "Status", "Action"];
    const rows = this.props.Products.products.rows;
    const fileUrl = this.props.Data.image.fileUrl;
    return (
      <div>
        {this.props.Products.error !== null ? (
          <UnAuthorized />
        ) : this.loading ? (
          <Loader loading={this.loading} />
        ) : (
          <div>
            <Card>
              <CardHeader>Products</CardHeader>
              {rows !== undefined ? (
                <Table responsive striped>
                  <thead>
                    <tr>
                      {columns.map((item, index) => (
                        <th key={index}>{item}</th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {rows.map((row, index) => (
                      <tr key={index}>
                        <td>{row.goodsName}</td>
                        <td>{row.description}</td>
                        <td>{row.status === 1 ? "Active" : "Inactive"}</td>
                        <td>
                          <Button
                            color="success"
                            onClick={e => this.togglePopup(row)}
                          >
                            Edit
                          </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              ) : (
                ""
              )}
            </Card>
            {this.state.isOpen && (
              <EditProductModal
                isShow={this.state.isOpen}
                togglePopup={this.togglePopup}
                item={this.state.rowData}
                fileUrl={fileUrl}
                updateProduct={this.updateProduct}
                updateProductTitle={this.updateProductTitle}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}
