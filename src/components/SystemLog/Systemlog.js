


import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Input, Button, Table } from 'reactstrap';
import Pagination from 'react-js-pagination';
import ViewSystemLogs from './ViewSystemLogs';
export default class SystemLog extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: false,
            activePage: 1,
            searchText: "",
            isViewOpen: false,
            rowData: {},
            isConfirm: false
        }
    }

    componentDidMount() {
        this.props.getSystemLogs(1, 5, "", this.props.token);
    }

    delete = (row) => {
        this.setState({
            isConfirm: true
        })
    }
    cancel = () => {
        this.setState({
            isConfirm: false
        })
    }
    changeInput = (e) => {
        if (e.target.value.length > 3) {
            this.props.getSystemLogs(this.state.activePage, 5, e.target.value)
        } else if (e.target.value.length === 0) {
            this.props.getSystemLogs(this.state.activePage, 5, "")
        }
        this.setState({
            searchText: e.target.value
        })
    }
    togglePopup = (row) => {
        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row
        })
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
        this.loading = true;
        this.props.getSystemLogs(pageNumber, 5, this.state.searchText, this.props.token);
        this.loading = false;
    }
    getDateFormat = (date) => {
        var d = new Date(date);
        var n = d.toDateString();
        var f = d.toLocaleTimeString();
        return n + " " + f;
    }
    render() {
        const columns = ['User Name', 'Date and Time of action', 'Actions']
        const rows = this.props.Systemlogs.SystemLogs.rows
        const totalCount = this.props.Systemlogs.SystemLogs.count;
        const totalPages = Math.floor(this.props.Systemlogs.SystemLogs.count) / 5;

        return (
            <div>
                <div>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <Input style={{ width: "200px", marginBottom: "10px" }} placeholder="Search..." onChange={(e) => this.changeInput(e)} value={this.state.searchText} onFocus></Input>
                    </div>
                    <Card>
                        <CardHeader>
                            System Logs
                    </CardHeader>
                        {rows !== undefined ?
                            <Table responsive striped>
                                <thead>
                                    <tr>
                                        {columns.map((item) =>
                                            <th>{item}</th>
                                        )} </tr>
                                </thead>
                                <tbody>{rows.length !== 0 ? rows.map((row) => <tr><td><Button color="link" onClick={() => this.togglePopup(row)}>{row.userId}</Button></td>
                                    <td>{this.getDateFormat(row.createdAt)}</td>
                                    <td>{row.message}</td>
                                </tr>)
                                    : <tr><td colSpan='5' style={{ textAlign: 'center' }}>No data available.</td></tr>}
                                </tbody></Table>
                            : ""}
                        <CardBody>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={5}
                                totalItemsCount={totalCount}
                                pageRangeDisplayed={totalPages}
                                itemClass="page-item"
                                linkClass="page-link"
                                onChange={(activePage) => this.handlePageChange(activePage)}
                            />
                        </CardBody>
                    </Card>
                    {this.state.isOpen && <ViewSystemLogs isShow={this.state.isOpen} togglePopup={this.togglePopup} item={this.state.rowData} />}
                </div >
            </div>
        )
    }
}