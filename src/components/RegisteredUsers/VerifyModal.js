import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Table,
  Row,
  Col,
  Container,
  Label,
  Input
} from "reactstrap";
import ViewDocument from "./ViewDocument";
export default class VerifyModal extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      fileType: ""
    };
  }
  toggle = () => {
    this.props.togglePopup();
  };
  viewDocument = (row, fileType) => {
    this.props.fetchDocument(row);
    this.setState({
      isOpen: true,
      fileType: fileType
    });
  };
  close = () => {
    this.setState({
      isOpen: false
    });
  };
  verify = () => {
    this.props.verify();
  };
  getDateFormat = date => {
    var d = new Date(date);
    return d.toLocaleDateString();
  };
  render() {
    const { item, fileData } = this.props;
    const childRows = item != {} ? this.props.item.documents.rows : [];
    const row = item != {} ? this.props.item : [];
    console.log(childRows);
    const childColumns = ["Document title", "Link"];
    return (
      <div>
        <Modal
          isOpen={this.props.isShow}
          toggle={() => this.toggle()}
          className={this.props.className}
        >
          <ModalHeader toggle={() => this.toggle()}>View Documents</ModalHeader>
          <ModalBody>
            {childRows.length !== 0 ? (
              <div>
                <Container>
                  <Row>
                    <Col xs="4">
                      <Label>Name</Label>
                    </Col>
                    <Col xs="auto">{row.fullName}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Email</Label>
                    </Col>
                    <Col xs="auto">{row.email}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Mobile Number</Label>
                    </Col>
                    <Col xs="auto">{row.mobileNumber}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Type of Signup</Label>
                    </Col>
                    <Col xs="auto">
                      {row.signUpOptions === 1 ? "Singpass" : "Manual"}
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Registration Date</Label>
                    </Col>
                    <Col xs="auto">{this.getDateFormat(row.createdAt)}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Status</Label>
                    </Col>
                    <Col xs="auto">
                      {row.status === 1 ? "Active" : "Inactive"}
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Occupation</Label>
                    </Col>
                    <Col xs="auto">{row.occupationName}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Employer</Label>
                    </Col>
                    <Col xs="auto">{row.employerName}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Mailing Address</Label>
                    </Col>
                    <Col xs="auto">{row.mailingAddress}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Residential Address</Label>
                    </Col>
                    <Col xs="auto">{row.residentialAddress}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Residential status</Label>
                    </Col>
                    <Col xs="auto">{row.residentialStatus}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Country code for phone number</Label>
                    </Col>
                    <Col xs="auto">{row.countryCode}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Nationality</Label>
                    </Col>
                    <Col xs="auto">{row.nationality}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Country of birth</Label>
                    </Col>
                    <Col xs="auto">{row.countryOfBirth}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>Date of birth</Label>
                    </Col>
                    <Col xs="auto">{row.dateOfBirth}</Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <Label>NRIC number</Label>
                    </Col>
                    <Col xs="auto">{row.certiNo}</Col>
                  </Row>
                </Container>
                <Table responsive striped>
                  <thead>
                    <tr>
                      {childColumns.map((column, index) => (
                        <th key={index}>{column}</th>
                      ))}{" "}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style={{ verticalAlign: "middle" }}>NRIC Front</td>
                      <td>
                        <Button
                          color="link"
                          onClick={() =>
                            this.viewDocument(
                              childRows[0].nricFront,
                              childRows[0].nricFrontMimeType
                            )
                          }
                        >
                          View Document
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ verticalAlign: "middle" }}>NRIC Back</td>
                      <td>
                        <Button
                          color="link"
                          onClick={() =>
                            this.viewDocument(
                              childRows[0].nricBack,
                              childRows[0].nricBackMimeType
                            )
                          }
                        >
                          View Document
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ verticalAlign: "middle" }}>User Photo</td>
                      <td>
                        <Button
                          color="link"
                          onClick={() =>
                            this.viewDocument(
                              childRows[0].userPhoto,
                              childRows[0].userPhotoMimeType
                            )
                          }
                        >
                          View Document
                        </Button>
                      </td>
                    </tr>
                    {childRows[0].proofOfAddress !== null &&
                    childRows[0].proofOfAddress !== "" ? (
                      <tr>
                        <td style={{ verticalAlign: "middle" }}>
                          Proof of ASSddress
                        </td>
                        <td>
                          <Button
                            color="link"
                            onClick={() =>
                              this.viewDocument(
                                childRows[0].proofOfAddress,
                                childRows[0].proofOfAddressMimeType
                              )
                            }
                          >
                            View Document
                          </Button>
                        </td>
                      </tr>
                    ) : (
                      ""
                    )}
                  </tbody>
                </Table>
              </div>
            ) : (
              <div>
                There are no documents uploaded. User registered via SingPass
              </div>
            )}
          </ModalBody>
        </Modal>
        {this.state.isOpen && fileData !== undefined ? (
          <ViewDocument
            isOpen={this.state.isOpen}
            close={this.close}
            fileData={fileData}
            fileType={this.state.fileType}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}
