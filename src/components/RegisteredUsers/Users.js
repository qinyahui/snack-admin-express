import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Input, Button, Table } from 'reactstrap';
import Pagination from "react-js-pagination";
import VerifyModal from './VerifyModal';
import Confirmation from "./Confirmation";
import Loader from '../Loader/Loader';
import UnAuthorized from '../../UnAuthorized/UnAuthorized';

export default class Users extends Component {
    constructor() {
        super();
        this.loading = true;
        this.state = {
            searchText: '',
            isOpen: false,
            activePage: 1,
            isConfirm: false,
            action: "",
            rowData: {}
        }
    }
    componentDidMount = async()=> {
        this.loading =  true;
        await this.props.getUsers(this.state.activePage, 20, "",this.props.token);
        this.loading =  false;

    }
    changeInput = (e) => {
        if (e.target.value.length > 3) {
            this.loading =  true;
             this.props.getUsers(this.state.activePage, 20, e.target.value,this.props.token)
            this.loading =  false;

        } else if(e.target.value.length === 0){
            this.loading =  true;
             this.props.getUsers(this.state.activePage, 20, "",this.props.token)
            this.loading =  false;

        }
        this.setState({
            searchText: e.target.value
        })
    }
    togglePopup = (row) => {
        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row
        })
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
        this.props.getUsers(pageNumber, 20, this.state.searchText,this.props.token);
    }
    fetchDocument = (row) => {
        this.props.fetchDocuments(row,this.props.token);
    }
    cancel = () => {
        this.setState({
            isConfirm: false
        })
    }

    verifyDocs = (action) => {
        this.loading = true;
        if (action == "verify") {
            const data = {
                "channelUserId": this.state.rowData.channelUserId,
                "verified": 1
            }
            this.props.verifyDocuments(data,this.props.token);

        } else if (action == "reject") {
            const data = {
                "channelUserId": this.state.rowData.channelUserId,
                "verified": 2
            }
            this.props.verifyDocuments(data,this.props.token);
        }
        setTimeout(() => {
            this.props.getUsers(this.state.activePage, 20, "",this.props.token);
            this.loading = false;
        }, 2000)
        this.setState({
            isConfirm: !this.state.isConfirm
        })
    }
    
    verify = (row, action) => {
        this.setState({
            isConfirm: !this.state.isConfirm,
            action: action,
            rowData: row
        })

    }
    statusButtons = (row) => {
        if (row.documentsVerified === 1) {
            return (<div>
                <Button color="success" onClick={e => this.togglePopup(row)} style={{ marginRight: "11px" }}>View</Button>
                <Button color="success" disabled>Verified</Button>
            </div>)


        } else if (row.documentsVerified === 2) {
            return (<div>
                <Button color="success" onClick={e => this.togglePopup(row)} style={{ marginRight: "11px" }}>View</Button>
                <Button color="danger" disabled>Rejected</Button>
           </div>)

        } else {
            return (
                <div><Button color="success" onClick={e => this.togglePopup(row)} style={{ marginRight: "11px" }}>View</Button>
                    <Button color="success" onClick={e => this.verify(row, "verify")} style={{ marginRight: "11px" }}>Verify</Button>
                    <Button color="danger" onClick={e => this.verify(row, "reject")}>Reject</Button></div>

            )
        }

    }
    getDateFormat = (date) =>{
        var d = new Date(date);
        return d.toLocaleDateString();
    }
    render() {
        const columns = ['User Name', 'Email Address', 'Phone Number', 'Registration Date',"Type of Signup",'Action']
        const rows = this.props.Users.users !== undefined ? this.props.Users.users.rows : [];
        const totalCount = this.props.Users.users !== undefined ? this.props.Users.users.count : 0;
        const totalPages = this.props.Users.users !== undefined ? Math.floor(this.props.Users.users.count / 20) : 0
        const fileData = this.props.Users.documents.fileData;
        return (
            <div>{this.props.Users.error !== null ? <UnAuthorized />: 
                this.loading ?
                    
                    <Loader loading={this.loading}/>:
                    <div>
                <Input style={{ width: "200px", marginBottom: "10px" }} placeholder="Search..." onChange={(e) => this.changeInput(e)} value={this.state.searchText} autoFocus></Input>
                <Card>
                    <CardHeader>
                        Users
                </CardHeader>
                    {rows !== undefined ?
                        <Table responsive striped>
                            <thead>
                                <tr>
                                    {columns.map((item) =>
                                        <th>{item}</th>
                                    )} </tr>
                            </thead>
                            <tbody>{
                                rows.map((row) => <tr><td style={{width: "30%"}}>{row.fullName}</td>
                                    <td style={{paddingRight: "0px"}}>{row.email}</td>
                                    <td>{row.mobileNumber}</td>
                            <td>{this.getDateFormat(row.createdAt)}</td>
                            
                                    <td>{row.signUpOptions === 1 ? "Singpass" : "Manual"}</td>
                                    <td>
                                        {this.statusButtons(row)}
                                    </td>

                                </tr>)
                            }
                            </tbody></Table>
                        : ""}
                    <CardBody>
                        <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={20}
                            totalItemsCount={totalCount}
                            pageRangeDisplayed={totalPages}
                            itemClass="page-item"
                            linkClass="page-link"
                            onChange={(activePage) => this.handlePageChange(activePage)}
                        />
                    </CardBody>
                </Card>
                <Confirmation verify={this.verify} isConfirm={this.state.isConfirm} action={this.state.action} cancel={this.cancel} verifyDocs={this.verifyDocs} />
                {this.state.isOpen && <VerifyModal isShow={this.state.isOpen} togglePopup={this.togglePopup} fetchDocument={this.fetchDocument} item={this.state.rowData} fileData={fileData} />}</div>}
            </div>
        );
    }
}