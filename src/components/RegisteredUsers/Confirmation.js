import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class Confirmation extends Component{

    toggle = () =>{
        this.props.verifyDocs(this.props.action);
    }
    cancel = () =>{
        this.props.cancel();
    }
    render(){
        return(
            <div>
                <Modal isOpen={this.props.isConfirm} className={this.props.className}>
                    <ModalHeader>Confirmation</ModalHeader>
                    <ModalBody>
                        Do you want to verify/reject this user?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={e => this.toggle()}>Yes</Button>
                        <Button color="secondary" onClick={() => this.cancel()}>No</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
} 