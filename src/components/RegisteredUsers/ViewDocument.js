import React, { Component } from 'react';
import {Modal, ModalHeader, ModalBody} from 'reactstrap';

export default class ViewDocument extends Component {
    
    toggle = () => {
        this.props.close()
    }

    render() {
        const src=`data:${this.props.fileType};base64,${this.props.fileData}`;
        return (
            <div>
                <Modal isOpen={this.props.isOpen} toggle={() => this.toggle()} className={this.props.className}>
                    <ModalHeader toggle={() => this.toggle()}>View document</ModalHeader>
                    <ModalBody>
                        {this.props.fileType === "application/pdf" && <iframe style={{width: "100%"}} src={src}></iframe>}
                        {this.props.fileType.includes("image") && <img style={{maxWidth: "100%", maxHeight:"100%"}} id="image" src={src}></img>}
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}
