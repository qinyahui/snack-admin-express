import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Row, Col, Label, Input } from 'reactstrap';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import TextEditor from './TextEditor';

export default class CreateFAQ extends Component {
    constructor(props) {
        super(props)
        this.state = {
            active: props.item !== undefined ? (props.item.status === 1 ? "yes" : "no") : "yes",
            faqTitle: props.item !== undefined ? props.item.title : "",
            question: props.item !== undefined ? props.item.question : "",
            answer: props.item !== undefined ? props.item.answer : "",
            category: "",
            dropdownOpen: false,
            value: props.item !== undefined ? props.item.categoryName : "",
            selectedId: props.item !== undefined ? props.item.categoryid : ""
        }
    }
    changeInputText = (e) => {
        const name = e.target.name
        this.setState({
            [name]: e.target.value
        })
    }
    togglePopup = () => {
        this.props.togglePopup();
    }
    getContent = (name, data) => {
        this.setState({
            [name]: data
        });
    }
    check = (e) => {
        this.setState({
            active: e.target.value,
        })
    }
    createFAQ = () => {
        const data = {
            "categoryid": this.state.selectedId,
            "question": this.state.question,
            "answer": this.state.answer,
            "status": this.state.active === "yes" ? 1 : 0,
        }
        this.props.createFAQs(data);
    }
    updateFAQ = () => {
        const data = {
            "id": this.props.item.id,
            "categoryid": this.state.selectedId,
            "question": this.state.question,
            "answer": this.state.answer,
            "status": this.state.active === "yes" ? 1 : 0,
        }
        this.props.updateFAQs(data);
    }
    toggleDropDown = (e) => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        })
    }
    select = (event, id) => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
            value: event.target.innerText,
            selectedId: id
        });
    }
    render() {
        return (
            <div>
                <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
                    <ModalHeader toggle={() => this.togglePopup()}>{this.props.title !== undefined ? `${this.props.title} FAQ` : 'Create FAQ'}</ModalHeader>
                    <ModalBody>
                        <Container>
                            <Row>
                                <Col xs='4'><Label>Category</Label></Col>
                                <Col xs='5'>
                                    <Dropdown isOpen={this.state.dropdownOpen} toggle={(e) => this.toggleDropDown(e)}>
                                        <DropdownToggle caret>
                                            {this.state.value !== "" ? this.state.value : "Category"}
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            {this.props.categories.map((cat) =>
                                                <DropdownItem value={cat.id} onClick={(e) => this.select(e, cat.id)}>{cat.categoryName}</DropdownItem>
                                            )}
                                        </DropdownMenu>
                                    </Dropdown>
                                </Col>

                                <Col xs='4'><Label>Question</Label></Col>
                                <Col xs='4'>
                                    <TextEditor getContent={this.getContent} name="question" data={this.state.question} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Answer</Label></Col>
                                <Col xs='4'>
                                    <TextEditor getContent={this.getContent} name="answer" data={this.state.answer} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Status</Label></Col>
                                <Col xs='auto' style={{marginLeft: "18px"}}>
                                    <Label check>
                                        <Input type="radio" name="radio1" value="yes" checked={this.state.active === "yes"} onChange={(e) => this.check(e)} />{' '}
                                        Active
                                     </Label></Col>
                                <Col>
                                    <Label check>
                                        <Input type="radio" name="radio1" value="no" checked={this.state.active === "no"} onChange={(e) => this.check(e)} />{' '}
                                        Inactive
                                     </Label>
                                </Col>
                            </Row>
                        </Container>
                    </ModalBody>
                    <ModalFooter>
                        {this.props.item !== undefined ? <Button color="success" onClick={() => this.updateFAQ()}>Update</Button>
                            : <Button color="success" onClick={() => this.createFAQ()}>Create</Button>}
                        <Button color="secondary" onClick={() => this.togglePopup()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}
