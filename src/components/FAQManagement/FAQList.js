import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Input, Button, Table } from 'reactstrap';
import Pagination from "react-js-pagination";
import CreateFAQ from './CreateFAQ';
import ViewFAQs from './ViewFAQs';
import Loader from '../Loader/Loader';
import UnAuthorized from '../../UnAuthorized/UnAuthorized';

export default class FAQList extends Component {
    constructor() {
        super()
        this.loading = true;
        this.state = {
            isOpen: false,
            activePage: 1,
            searchText: "",
            isViewOpen: false,
            rowData: {},
            isConfirm: false,
            title: ""
        }
    }

    togglePopup = (row, title) => {

        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row,
            title: title
        })
    }
    componentDidMount() {
        this.loading = true;
        this.props.getFAQs(1, 5, "", this.props.token);
        this.props.getFAQCategories(1, 10, "",this.props.token);
        this.loading = false;
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
        this.loading = true;
        this.props.getFAQs(pageNumber, 5, this.state.searchText,this.props.token);
        this.loading = false;
    }
    changeInput = (e) => {
        if (e.target.value.length > 3) {
            this.loading = true;
            this.props.getFAQs(this.state.activePage, 5, e.target.value,this.props.token)
            this.loading =  false;
        } else if(e.target.value.length === 0){
            this.loading = true;
            this.props.getFAQs(this.state.activePage, 5, "",this.props.token)
            this.loading =  false; 
        }
        this.setState({
            searchText: e.target.value
        })
    }
    createFAQs = (params) =>{
        this.loading = true;
        this.props.createFAQs(params, this.props.token);
        setTimeout(()=>{
            this.props.getFAQs(this.state.activePage, 5, "",this.props.token)
            this.loading = false;
        }, 3000)
        this.setState({
            isOpen: !this.state.isOpen,
        })
    }
    updateFAQs = (params) =>{
        this.loading = true;
        this.props.updateFAQs(params,this.props.token);
        setTimeout(()=>{
            this.props.getFAQs(this.state.activePage, 5, "",this.props.token)
            this.loading =  false;
        }, 3000)
        this.setState({
            isOpen: !this.state.isOpen,
        })
    }
    toggleView = (row) =>{
        this.setState({
            isViewOpen: !this.state.isViewOpen,
            rowData: row
        })
    }
    
    render() {
        const columns = ['Category', 'Question', 'Answers', 'Actions']
        const rows = this.props.FAQs !== undefined ? this.props.FAQs.FAQ.rows : [];
        const totalCount = this.props.FAQs.FAQ.count;
        const totalPages = Math.floor(this.props.FAQs.FAQ.count) / 5;
        const categories = this.props.FAQs.categories.rows;
        return (
            <div>{this.props.FAQs.error !== null ? <UnAuthorized/>: 
                this.loading ? <Loader loading={this.loading}/> : 
            <div>
                <div style={{display: "flex", justifyContent: "space-between"}}>
                <Input style={{ width: "200px", marginBottom: "10px" }} placeholder="Search..." onChange={(e) => this.changeInput(e)} value={this.state.searchText} autoFocus></Input>
                <Button color="primary" style={{marginBottom: "10px"}} className="" onClick={()=>this.togglePopup()}>Create FAQ</Button>
                </div>
                <Card>
                    <CardHeader>
                        FAQ
                        </CardHeader>
                    {rows !== undefined ?
                        <Table responsive striped>
                            <thead>
                                <tr>
                                    {columns.map((item) =>
                                        <th>{item}</th>
                                    )} </tr>
                            </thead>
                            <tbody>{rows.length !== 0 ? rows.map((row) => <tr>
                                    <td>{row.categoryName}</td>
                                    <td style={{ width: "20%" }}>{row.question !== null && row.question.replace(/(<([^>]+)>)/ig,"")}</td>
                                    <td style={{ width: "45%" }}>{row.answer !== null &&row.answer.replace(/(<([^>]+)>)/ig,"")}</td><td>
                                        {/* <Button color="success" style={{ marginRight: "15px" }} onClick={e => this.toggleView(row)}>View</Button> */}
                                        <Button color="success" style={{ marginRight: "15px" }} onClick={e => this.togglePopup(row, 'Edit')}>Edit</Button>
                                        {/* <Button color="success" onClick={e => this.togglePopup(row)}>Delete</Button> */}
                                    </td></tr>)
                            :<tr><td colSpan='5' style={{textAlign: 'center'}}>No data available.</td></tr>}
                            </tbody></Table>
                        : ""}
                    <CardBody>
                        <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={5}
                            totalItemsCount={totalCount}
                            pageRangeDisplayed={totalPages}
                            itemClass="page-item"
                            linkClass="page-link"
                            onChange={(activePage) => this.handlePageChange(activePage)}
                        />
                    </CardBody>
                </Card>
                {this.state.isViewOpen && <ViewFAQs isShow={this.state.isViewOpen} toggleView={this.toggleView} item={this.state.rowData}/>}
                {this.state.isOpen && <CreateFAQ isShow={this.state.isOpen} togglePopup={this.togglePopup} createFAQs={this.createFAQs} item={this.state.rowData} updateFAQs={this.updateFAQs} categories={categories} title={this.state.title}/>}</div>}
                </div>
        )
    }
}