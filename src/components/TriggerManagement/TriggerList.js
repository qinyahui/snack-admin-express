import React, { Component } from 'react';
import { Card, CardHeader, Button, Table } from 'reactstrap';
import EditTriggerModal from './EditTriggerModal';
import Loader from '../Loader/Loader';
import UnAuthorized from '../../UnAuthorized/UnAuthorized';

export default class TriggerList extends Component {
    constructor() {
        super();
        this.loading = true;
        this.state = {
            isOpen: false,
            activePage: 1,
            rowData: {}

        }
    }
    togglePopup = (row) => {
        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row
        })
    }
    componentDidMount = () => {
        this.props.getTriggers(1, 6, "", this.props.token);
        this.loading = false;
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
    }
    updateTrigger = (data) => {
        this.loading = true;
        this.props.updateTriggers(data, this.props.token);
        setTimeout(() => {
            this.props.getTriggers(this.state.activePage, 10, "", this.props.token);
            this.loading = false;
        }, 3000)
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    render() {
        const columns = ['Name', 'Description', 'Status', 'Action']
        const rows = this.props.Triggers.triggers !== undefined ? this.props.Triggers.triggers.rows : [];
        const fileUrl = "";
        return (
            <div>{this.props.Triggers.error !== null ? <UnAuthorized/>: 
                this.loading ?

                    <Loader loading={this.loading} /> :
                    <div>
                        <Card>
                            <CardHeader>
                                Triggers list
                    </CardHeader>
                            {rows !== undefined ?
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            {columns.map((item) =>
                                                <th>{item}</th>
                                            )} </tr>
                                    </thead>
                                    <tbody>{
                                        rows.map((row) => <tr><td>{row.triggerCategoryName}</td>
                                            <td>{row.remarks}</td>
                                            <td>{row.status === 1 ? "Active" : "Inactive"}</td><td>
                                                <Button color="success" onClick={e => this.togglePopup(row)}>Edit</Button>
                                            </td></tr>)
                                    }
                                    </tbody></Table>
                                : ""}
                        </Card>
                        <EditTriggerModal isShow={this.state.isOpen} togglePopup={this.togglePopup} item={this.state.rowData} fileUrl={fileUrl} updateTrigger={this.updateTrigger} /></div>}
            </div>
        );
    }
}