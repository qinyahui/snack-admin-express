import React, { Component } from 'react';
import { css } from "@emotion/core";
import { BeatLoader
} from "react-spinners";

const override = css`
  // display: block;
  // margin: 0 auto;
  // border-color: red;
`;
export default class Loader extends Component{
    constructor(){
        super()
    }
    render(){
        return(
            <div className="sweet-loading">
            <BeatLoader
              css={override}
              //size={"150px"} this also works
              color={"#20a8d8"}
              loading={this.props.loading}
            />
          </div>
        )
    }
}