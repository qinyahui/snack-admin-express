import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Input, Row, Col } from 'reactstrap';


export default class CreateTandC extends Component {
    constructor(){
        super()
        this.state={
            dynamicRows: [{headerLabel: 'Header', headerData: '', contentLabel: 'Content', contentData:''}]
        }
    }
    togglePopup = () =>{
        this.props.togglePopup();
    }
    addRows = () =>{
        // const {dynamicRows} = this.state;
       
    }
    render(){
        const {dynamicRows} = this.state;
        return(
            <div>
                  <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
                    <ModalHeader toggle={() => this.togglePopup()}></ModalHeader>
                    <ModalBody>
                            <Container>
                                {dynamicRows.map(()=>
                                 <div><Row>
                                   <Col xs="3">Header</Col>
                                   <Col xs="3"><Input></Input></Col>
                               </Row>
                               <Row>
                                   <Col xs="3">Content</Col>
                                   <Col xs="3"><Input></Input></Col>
                               </Row></div>
                                )

                                }

                              
                            </Container> 
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success">+Add</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}