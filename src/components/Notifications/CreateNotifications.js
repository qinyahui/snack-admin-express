import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Input, Row, Col, Label } from 'reactstrap';
import axios from 'axios';
import blank_img from '../../assets/img/brand/blank_img.png';
import CONFIG from '../../lib/config';

export default class CreateNotifications extends Component {
    constructor(props){
        super(props)
        this.state={
            notificationTitle: props.item !== undefined ? props.item.announcementTitle : "",
            date: props.item !== undefined ? props.item.scheduledDate : "",
            time: props.item !== undefined ? props.item.scheduledTime : "",
            description: props.item !== undefined ? props.item.description : "",
            imagePreviewUrl: props.item !== undefined ? props.item.imageUrl : "",
            imageName: "",
        }
    }
    changeInputText =(e) =>{
       const name = e.target.name
       this.setState({
           [name]: e.target.value
       })
    }
    togglePopup = () =>{
        this.props.togglePopup();
    }
    CreateNotification = () =>{
        const params = {
            "announcementTitle": this.state.notificationTitle,
            "description": this.state.description,
            "scheduledDate": this.state.date,
            "scheduledTime": this.state.time,
            "imageUrl": `${this.state.imageName}`
        }
        this.props.CreateNotification(params);
    }
    updateNotification = () =>{
        const params = {
            "id" : this.props.item.id,
            "announcementTitle": this.state.notificationTitle,
            "description": this.state.description,
            "scheduledDate": this.state.date,
            "scheduledTime": this.state.time,
            "imageUrl": this.state.imageName !== "" ? this.state.imageName : (this.props.item.imageUrl !== null ? this.props.item.imageUrl.slice(60,this.props.item.imageUrl.length): null),
        }
        this.props.updateNotification(params);
    }
    _handleSubmit(e) {
        e.preventDefault();
        // TODO: do something with -> this.state.file
        // let formData = new FormData();
        // formData.append('file', this.state.file);
        // formData.append('fileType', "photo");
        // axios.post('http://upload-snack.13.251.251.232.nip.io/upload',
        //     formData,
        //     {
        //         headers: {
        //             'Content-Type': 'multipart/form-data'
        //         }
        //     }
        // ).then((res)=> {
        //     this.setState({
        //         imageName: res.data.fileName,
        //     })
        //     console.log('SUCCESS!!', res);

        // })
        //     .catch(function () {
        //         console.log('FAILURE!!');
        //     });
    }

    _handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
                flag: true
            });
        }

        reader.readAsDataURL(file)
        let formData = new FormData();
        formData.append('fileType', "photo");
        formData.append('fileName',file.name);
        formData.append('file', file);
        axios.post(CONFIG.documentFetchUrl+'/upload',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then((res)=> {
            this.setState({
                imageName: res.data.fileName,
            })
            console.log('SUCCESS!!', res);

        })
            .catch(function () {
                console.log('FAILURE!!');
            });
    }
render(){
    let { imagePreviewUrl } = this.state;
    return(
        <div>
            <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
    <ModalHeader toggle={() => this.togglePopup()}>{this.props.title !== undefined ? `${this.props.title} Notification` : "Create Notification"}</ModalHeader>
                    <ModalBody>
                            <Container>
                                <Row>
                                    <Col xs='4'><Label>Notification Title</Label></Col>
                                    <Col xs='auto'>
                                        <Input
                                            onChange={(e) => this.changeInputText(e)}
                                            name="notificationTitle"
                                            value={this.state.notificationTitle}>
                                        </Input>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs='4'><Label>Scheduled Date</Label></Col>
                                    <Col xs='5'>
                                        <Input
                                            className="date"
                                            type="date"
                                            onChange={(e) => this.changeInputText(e)}
                                            name="date"
                                            // style={{ height: '100px' }}
                                            value={this.state.date}>
                                        </Input>
                                    </Col>

                                    <Col xs='4'><Label>Scheduled Time</Label></Col>
                                    <Col xs='auto'>
                                        <Input
                                            onChange={(e) => this.changeInputText(e)}
                                            name="time"
                                            type="time"
                                            // style={{ height: '100px' }}
                                            value={this.state.time}>
                                        </Input>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs='4'><Label>Description</Label></Col>
                                    <Col xs='auto'>
                                        <Input
                                            onChange={(e) => this.changeInputText(e)}
                                            type="textarea"
                                            name="description"
                                            style={{ height: '100px' }}
                                            value={this.state.description}>
                                        </Input>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs='4'><Label> Image upload</Label></Col>
                                    <Col xs='4'><form onSubmit={(e) => this._handleSubmit(e)} style={{display: "flex"}}>
                                        <input className="fileInput"
                                            id="file_input"
                                            type="file"
                                            onChange={(e) => this._handleImageChange(e)}
                                            style={{display: 'none'}} />
                                        {/* <button className="submitButton"
                                            type="submit"
                                            onClick={(e) => this._handleSubmit(e)} className="upload_btn">Upload</button> */}
                                    </form>
                                        <img className="portfoilio-img" alt="image" src={imagePreviewUrl && imagePreviewUrl !== null ? imagePreviewUrl : blank_img} onClick={()=>document.getElementById('file_input').click()}></img></Col>
                                </Row>
                            </Container> 
                            {/* : ""} */}
                    </ModalBody>
                    <ModalFooter>
                        {this.props.item !== undefined ? <Button color="success" onClick={() => this.updateNotification()}>Update</Button>
                        :<Button color="success" onClick={() => this.CreateNotification()}>Create</Button>}
                        <Button color="secondary" onClick={() => this.togglePopup()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
        </div>
    )
}
}
