import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Container, Row, Col, Label } from 'reactstrap';


export default class ViewNotifications extends Component {
    constructor() {
        super()
        this.state = {
            notificationTitle: "",
            date: "",
            time: ""
        }
    }
    togglePopup = () => {
        this.props.togglePopup();
    }
    render() {
        const { item } = this.props;
        return (
            <div>
                <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
                    <ModalHeader toggle={() => this.togglePopup()}>Notification</ModalHeader>
                    <ModalBody>
                        {/* {item !== undefined ? */}
                        <Container>
                            <Row>
                                <Col xs='4'><Label>Notification Title</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.announcementTitle}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Scheduled Date</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.scheduledDate}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Scheduled Time</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.scheduledTime}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Description</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.description}</Label>
                                </Col>
                            </Row>
                        </Container>
                        {/* : ""} */}
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}
