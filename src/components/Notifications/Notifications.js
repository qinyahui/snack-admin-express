import React, { Component } from "react";
import { Card, CardBody, CardHeader, Input, Button, Table } from "reactstrap";
import Pagination from "react-js-pagination";
import CreateNotifications from "./CreateNotifications";
import ViewNotifications from "./ViewNotifications";
import Confirmation from "./Confirmation";
import Loader from "../Loader/Loader";
import UnAuthorized from "../../UnAuthorized/UnAuthorized";

export default class Notifications extends Component {
  constructor() {
    super();
    this.loading = true;
    this.state = {
      isOpen: false,
      activePage: 1,
      searchText: "",
      isViewOpen: false,
      rowData: {},
      isConfirm: false,
      title: ""
    };
  }
  componentDidMount() {
    this.loading = true;
    this.props.getNotifications(1, 5, "", this.props.token);
    this.loading = false;
  }
  togglePopup = (row, title) => {
    this.setState({
      isOpen: !this.state.isOpen,
      rowData: row,
      title: title
    });
  };
  toggleView = row => {
    this.setState({
      isViewOpen: !this.state.isViewOpen,
      rowData: row
    });
  };
  changeInput = e => {
    if (e.target.value.length > 3) {
      this.loading = true;
      this.props.getNotifications(
        this.state.activePage,
        5,
        e.target.value,
        this.props.token
      );
      this.loading = false;
    } else if (e.target.value.length === 0) {
      this.loading = true;
      this.props.getNotifications(
        this.state.activePage,
        5,
        "",
        this.props.token
      );
      this.loading = false;
    }
    this.setState({
      searchText: e.target.value
    });
  };
  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.loading = true;
    this.props.getNotifications(
      pageNumber,
      5,
      this.state.searchText,
      this.props.token
    );
    this.loading = false;
  }
  updateNotification = params => {
    this.loading = true;
    this.props.updateNotification(params, this.props.token);
    this.setState({
      isOpen: false
    });
    setTimeout(() => {
      this.props.getNotifications(
        this.state.activePage,
        5,
        "",
        this.props.token
      );
      this.loading = false;
    }, 3000);
  };
  CreateNotification = params => {
    this.loading = true;
    this.props.createNotification(params, this.props.token);
    this.setState({
      isOpen: false
    });
    setTimeout(() => {
      this.props.getNotifications(
        this.state.activePage,
        5,
        "",
        this.props.token
      );
      this.loading = false;
    }, 3000);
  };
  delete = row => {
    this.setState({
      isConfirm: true
    });
  };
  cancel = () => {
    this.setState({
      isConfirm: false
    });
  };
  getDateFormat = date => {
    var d = new Date(date);
    return d.toLocaleDateString();
  };
  render() {
    const columns = [
      "Notification Title",
      "Scheduled Date",
      "Scheduled Time",
      "Description",
      "Push Sent ",
      "Action"
    ];
    const rows = this.props.Notifications.notifications.rows;
    const totalCount = this.props.Notifications.notifications.count;
    const totalPages =
      Math.floor(this.props.Notifications.notifications.count) / 5;

    return (
      <div>
        {this.props.Notifications.error !== null ? (
          <UnAuthorized />
        ) : this.loading ? (
          <Loader loading={this.loading} />
        ) : (
          <div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Input
                style={{ width: "200px", marginBottom: "10px" }}
                placeholder="Search..."
                onChange={e => this.changeInput(e)}
                value={this.state.searchText}
              ></Input>
              <Button
                color="primary"
                style={{ marginBottom: "10px" }}
                className=""
                onClick={() => this.togglePopup()}
              >
                Create Notification
              </Button>
            </div>
            <Card>
              <CardHeader>Notifications</CardHeader>
              {rows !== undefined ? (
                <Table responsive striped>
                  <thead>
                    <tr>
                      {columns.map(item => (
                        <th>{item}</th>
                      ))}{" "}
                    </tr>
                  </thead>
                  <tbody>
                    {rows.length !== 0 ? (
                      rows.map(row => (
                        <tr>
                          <td>{row.announcementTitle}</td>
                          <td>{this.getDateFormat(row.scheduledDate)}</td>
                          <td>{row.scheduledTime}</td>
                          <td>{row.description}</td>
                          <td>{row.push_sent + ''}</td>
                          <td>
                            <Button
                              style={{ marginRight: "10px" }}
                              color="success"
                              onClick={() => this.toggleView(row)}
                            >
                              View
                            </Button>
                            {/* <Button style={{marginRight: "10px"}} color="success" onClick={()=> this.delete(row)}>Delete</Button> */}
                            <Button
                              color="success"
                              onClick={() => this.togglePopup(row, "Edit")}
                              disabled={
                                new Date(row.scheduledDate) < new Date()
                                  ? true
                                  : false
                              }
                            >
                              Edit
                            </Button>
                          </td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td colSpan="5" style={{ textAlign: "center" }}>
                          No data available.
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              ) : (
                ""
              )}
              <CardBody>
                <Pagination
                  activePage={this.state.activePage}
                  itemsCountPerPage={5}
                  totalItemsCount={totalCount}
                  pageRangeDisplayed={totalPages}
                  itemClass="page-item"
                  linkClass="page-link"
                  onChange={activePage => this.handlePageChange(activePage)}
                />
              </CardBody>
            </Card>
            <Confirmation
              delete={this.delete}
              isConfirm={this.state.isConfirm}
              cancel={this.cancel}
            />
            {this.state.isOpen && (
              <CreateNotifications
                isShow={this.state.isOpen}
                togglePopup={this.togglePopup}
                CreateNotification={this.CreateNotification}
                item={this.state.rowData}
                updateNotification={this.updateNotification}
                title={this.state.title}
              />
            )}
            {this.state.isViewOpen && (
              <ViewNotifications
                isShow={this.state.isViewOpen}
                togglePopup={this.toggleView}
                item={this.state.rowData}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}
