import React, { Component } from 'react';
import { Card, CardHeader, Button, Table } from 'reactstrap';
import EditTriggerSourceModal from './EditTriggerSourceModal';
import Loader from '../Loader/Loader';
import UnAuthorized from '../../UnAuthorized/UnAuthorized';
export default class TriggerSourceList extends Component {
    constructor() {
        super();
        this.loading = true;
        this.state = {
            isOpen: false,
            activePage: 1,
            rowData: {}
        }
    }
    togglePopup = (row) => {
        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row
        })
    }
    componentDidMount = () => {
        this.props.getTriggerSources(1, 5, "", this.props.token);
        this.loading = false;
    }
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
    }
    updateTriggerSource = (data) => {
        this.loading = true;
        this.props.updateTriggerSource(data, this.props.token);
        setTimeout(() => {
            this.props.getTriggerSources(this.state.activePage, 10, "", this.props.token);
            this.loading = false;
        }, 5000)
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    render() {
        const columns = ['Name', 'Description', 'Status', 'Action']
        const rows = this.props.TriggerSources.triggerSources.rows;
        const fileUrl = "";
        return (
            <div>{this.props.TriggerSources.error !== null ? <UnAuthorized />: 
                this.loading ?

                    <Loader loading={this.loading} /> :
                    <div>
                        <Card>
                            <CardHeader>
                                Triggers Sources list
                    </CardHeader>
                            {rows !== undefined ?
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            {columns.map((item) =>
                                                <th>{item}</th>
                                            )} </tr>
                                    </thead>
                                    <tbody>{
                                        rows.map((row) => <tr><td>{row.dataSourceName}</td>
                                            <td>{row.remarks}</td>
                                            <td>{row.status === 1 ? "Active" : "Inactive"}</td><td>
                                                <Button color="success" onClick={e => this.togglePopup(row)}>Edit</Button>
                                            </td></tr>)
                                    }
                                    </tbody></Table>
                                : ""}
                        </Card>
                        <EditTriggerSourceModal isShow={this.state.isOpen} togglePopup={this.togglePopup} item={this.state.rowData} fileUrl={fileUrl} updateTriggerSource={this.updateTriggerSource} /></div>}
            </div>
        );
    }
}