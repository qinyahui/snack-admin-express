import React, { Component } from 'react';
import {connect} from 'react-redux';
import  { Route, Switch } from 'react-router-dom';
import {checkAuthorization} from './actions/Common';
import './App.scss';
import UnAuthorized from './UnAuthorized/UnAuthorized';
import SessionExpired from './UnAuthorized/SessionExpired';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

class Home extends Component {
  constructor(props){
    super(props)
    const params = window.location.search;
    if(params){
      props.checkAuthorization(params);
      localStorage.setItem('userId', params);
    }
  }
  renderApp = () =>{
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    if(this.props.token == "" && userId){
      console.log('if', this.props);
      return <SessionExpired />
    } 
    else if(token !== undefined && token !== null){
      return(
        <Route path="/" name="Home" render={props => <DefaultLayout {...props} token={token}/>} />
      )
    }
    else{
      console.log('else', this.props);
      return <UnAuthorized /> 
    }
  }

  render() {
   
    
    return (
          <React.Suspense fallback={loading()}>
            <Switch>
              {this.renderApp()}
            </Switch>
          </React.Suspense>
    );
  }
}
const mapStateToProps = state => {
  return {
      token: state.CommonReducer.token
  };
};

const mapDispatchToProps = {
checkAuthorization
}
const App = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);

export default App;
