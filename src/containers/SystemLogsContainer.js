import {connect} from 'react-redux';
import {getSystemLogs} from '../actions/SystemLogs';
import SystemLog from '../components/SystemLog/Systemlog';

const mapStateToProps = state => {
    return {
        Systemlogs:state.SystemLogsReducer,
    };
};

const mapDispatchToProps = {
    getSystemLogs
}
const SystemLogsContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(SystemLog);


export default SystemLogsContainer;