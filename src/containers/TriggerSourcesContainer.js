import {connect} from 'react-redux';
import {getTriggerSources, updateTriggerSource} from '../actions/TriggerSource';
import {fetchImages} from '../actions/Common';
import TriggerSourceList from '../components/TriggerSourceManagement/TriggerSourceList';

const mapStateToProps = state => {
    return {
        TriggerSources:state.TriggerSourceReducer,
        Data: state.CommonReducer
    };
};

const mapDispatchToProps = {
    getTriggerSources,
    fetchImages,
    updateTriggerSource
}
const TriggerSourcesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TriggerSourceList);


export default TriggerSourcesContainer;