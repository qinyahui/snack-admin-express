import {connect} from 'react-redux';
import {getTandC} from '../actions/T&C';
import TandC from '../components/T&CManagement/TandC';

const mapStateToProps = state => {
    return {
        TandC:state.TandCReducer,
    };
};

const mapDispatchToProps = {
    getTandC
}
const TandCContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(TandC);


export default TandCContainer;