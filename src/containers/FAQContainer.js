import {connect} from 'react-redux';
import {getFAQs, createFAQs, updateFAQs, getFAQCategories} from '../actions/FAQ';
import FAQ from '../components/FAQManagement/FAQList';

const mapStateToProps = state => {
    return {
        FAQs:state.FAQReducer,
    };
};

const mapDispatchToProps = {
   getFAQs,
   createFAQs,
   updateFAQs,
   getFAQCategories
}
const FAQContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FAQ);


export default FAQContainer;