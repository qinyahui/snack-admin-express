import {connect} from 'react-redux';
import {getUsers, fetchDocuments, verifyDocuments} from '../actions/Users';
import Users from '../components/RegisteredUsers/Users';

const mapStateToProps = state => {
    return {
        Users:state.UserReducer,
    };
};

const mapDispatchToProps = {
    getUsers,
    fetchDocuments,
    verifyDocuments
}
const UserContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Users);


export default UserContainer;