import {connect} from 'react-redux';
import {getProducts, updateProducts,updateGoodsTitle} from '../actions/Product';
import {fetchImages} from '../actions/Common';
import ProductList from '../components/ProductManagement/ProductList';

const mapStateToProps = state => {
    return {
        Products:state.ProductReducer,
        Data: state.CommonReducer

    };
};

const mapDispatchToProps = {
    getProducts,
    fetchImages,
    updateProducts,
    updateGoodsTitle
}
const ProductContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductList);


export default ProductContainer;