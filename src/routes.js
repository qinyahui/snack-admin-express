import React from 'react';

// working routes
const SystemLog = React.lazy(()=> import('./containers/SystemLogsContainer'));
const FAQ = React.lazy(()=> import('./components/SystemLog/Systemlog'));
const TandC = React.lazy(()=> import('./containers/TandCContainer'));
const Notifications = React.lazy(()=> import('./containers/NotificationContainer'));
const RegisteredUsers = React.lazy(()=> import('./containers/UserContainer'));
const ProductList = React.lazy(()=> import('./containers/ProductContainer'));
const TriggerList = React.lazy(()=> import('./containers/TriggerContainer'));
const TriggerSourceList = React.lazy(()=> import ('./containers/TriggerSourcesContainer'));
const FAQList = React.lazy(()=> import('./containers/FAQContainer'));
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  // { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/systemLogs', name: 'SystemLog', component: SystemLog },
  { path: '/faq', name: 'FAQ', component: FAQList },
  { path: '/t&c', name: 'T&C', component: TandC},
  { path: '/notifications', name: 'Notifications', component: Notifications },
  { path: '/users', name: 'Users', component: RegisteredUsers},
  { path: '/edit/products', name: 'Products', component: ProductList},
  { path: '/edit/trigger', name: 'Trigger', component: TriggerList},
  { path: '/edit/triggersource', name: 'Trigger Source', component: TriggerSourceList},
];

export default routes;
