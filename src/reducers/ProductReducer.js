import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    products: [],
    error: null,
    updatedData:[],
    isProductFetch: null,
    isProductUpdate: null,
    isGoodstitileUpdate: null
  };

  export default function ProductReducer (state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_PRODUCT_STARTED:
        return {
          ...state,
          loading: true,
          isProductFetch: false
        };
      case types.GET_PRODUCT_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          products: action.payload,
          isProductFetch: true
        };
      case types.GET_PRODUCT_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isProductFetch: false
        };
        case types.UPDATE_PRODUCT_STARTED:
          return {
            ...state,
            loading: true,
            isProductUpdate: false
          };
        case types.UPDATE_PRODUCT_SUCCEEDED:
          return {
            ...state,
            loading: false,
            error: null,
            updatedData: action.payload,
            isProductUpdate: true
          };
        case types.UPDATE_PRODUCT_FAILED:
          return {
            ...state,
            loading: false,
            error: action.payload,
            isProductUpdate: false
          };
          case types.UPDATE_GOODS_TITLE_STARTED:
            return {
              ...state,
              loading: true,
              isGoodstitileUpdate: false
            };
          case types.UPDATE_GOODS_TITLE_SUCCEEDED:
            return {
              ...state,
              loading: false,
              error: null,
              updatedData: action.payload,
              isGoodstitileUpdate: true
            };
          case types.UPDATE_GOODS_TITLE_FAILED:
            return {
              ...state,
              loading: false,
              error: action.payload,
              isGoodstitileUpdate: false
            };
      default:
          return{
              ...state
          };
        }
    }