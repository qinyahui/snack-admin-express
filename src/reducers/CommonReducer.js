import * as types from '../lib/ActionTypes';

const initialState = {
  loading: false,
  image: "",
  error: null,
  token: "",
  isImageFetched: null,
  isTokenFetched: null
};

export default function CommonReducer(state, action) {
  if(typeof state === "undefined"){
    return initialState;
  }
  switch (action.type) {
    case types.FETCH_IMAGE_STARTED:
      return {
        ...state,
        loading: true,
        isImageFetched: false
      };
    case types.FETCH_IMAGE_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        image: action.payload,
        isImageFetched: true
      };
    case types.FETCH_IMAGE_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isImageFetched: false
      };
    case types.CHECK_AUTHORIZATION_STARTED:
      return {
        ...state,
        loading: true,
        isTokenFetched: false
      };
    case types.CHECK_AUTHORIZATION_SUCCEEDED:
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        loading: false,
        error: null,
        token: action.payload,
        isTokenFetched: true
      };
    case types.CHECK_AUTHORIZATION_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isTokenFetched: false
      };
    default:
      return {
        ...state
      };
  }
}