import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    notifications: [],
    notification:[],
    error: null,
    isNotificationFetched: null,
    isNotificationCreated: null
  };

  export default function NotificationReducer(state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_NOTIFICATIONS_STARTED:
        return {
          ...state,
          loading: true,
          isNotificationFetched: false
        };
      case types.GET_NOTIFICATIONS_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          notifications: action.payload,
          isNotificationFetched: true
        };
      case types.GET_NOTIFICATIONS_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isNotificationFetched: false
        };
      case types.CREATE_NOTIFICATIONS_STARTED:
          return {
            ...state,
            loading: true,
            isNotificationCreated: false
          };
      case types.CREATE_NOTIFICATIONS_SUCCEEDED:
          return {
            ...state,
            loading: false,
            error: null,
            notification: action.payload,
            isNotificationCreated: true
          };
      case types.CREATE_NOTIFICATIONS_FAILED:
          return {
            ...state,
            loading: false,
            error: action.payload,
            isNotificationCreated: false
          };
      case types.UPDATE_NOTIFICATIONS_STARTED:
              return {
                ...state,
                loading: true
              };
      case types.UPDATE_NOTIFICATIONS_SUCCEEDED:
            return {
              ...state,
              loading: false,
              error: null,
              notification: action.payload
            };
      case types.UPDATE_NOTIFICATIONS_FAILED:
            return {
              ...state,
              loading: false,
              error: action.payload
            };
      default:
          return{
              ...state
          };
        }
    }