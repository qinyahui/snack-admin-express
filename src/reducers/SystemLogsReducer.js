import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    SystemLogs: [],
    error: null,
    isSystemLogsFetch: null
  };

  export default function SystemLogsReducer(state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_SYSTEM_LOGS_STARTED:
        return {
          ...state,
          loading: true,
          isSystemLogsFetch: false
        };
      case types.GET_SYSTEM_LOGS_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          SystemLogs: action.payload,
          isSystemLogsFetch: true
        };
      case types.GET_SYSTEM_LOGS_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isSystemLogsFetch: false
        };
        // case types.UPDATE_TRIGGER_STARTED:
        //   return {
        //     ...state,
        //     loading: true
        //   };
        // case types.UPDATE_TRIGGER_SUCCEEDED:
        //   return {
        //     ...state,
        //     loading: false,
        //     error: null,
        //     updatedData: action.payload
        //   };
        // case types.UPDATE_TRIGGER_FAILED:
        //   return {
        //     ...state,
        //     loading: false,
        //     error: action.payload
        //   };
      default:
          return{
              ...state
          };
        }
    }