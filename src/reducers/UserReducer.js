import * as types from '../lib/ActionTypes';

const initialState = {
  loading: false,
  users: [],
  error: null,
  documents: [],
  verified: false,
  isUserFetch: null,
  isDocumentVerified: null
};

export default function UserReducer(state, action) {
  if(typeof state === "undefined"){
    return initialState;
  }
  switch (action.type) {
    case types.GET_USERS_STARTED:
      return {
        ...state,
        loading: true,
        isUserFetch: false
      };
    case types.GET_USERS_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        users: action.payload.users,
        isUserFetch: true
      };
    case types.GET_USERS_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isUserFetch: false
      };
    case types.GET_DOCUMENT_STARTED:
      return {
        ...state,
        loading: true
      };
    case types.GET_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        documents: action.payload
      };
    case types.GET_DOCUMENT_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case types.VERIFY_DOCUMENT_STARTED:
      return {
        ...state,
        loading: true,
        isDocumentVerified: false
      };
    case types.VERIFY_DOCUMENT_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        verified: action.payload,
        isDocumentVerified: true
      };
    case types.VERIFY_DOCUMENT_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isDocumentVerified: false
      };
    default:
      return {
        ...state
      };
  }
}