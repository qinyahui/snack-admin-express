import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    triggerSources: [],
    error: null,
    updatedData: [],
    isTriggerResourceFetch: null
  };

  export default function TriggerSourceReducer(state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_TRIGGER_SOURCES_STARTED:
        return {
          ...state,
          loading: true,
          isTriggerResourceFetch: false
        };
      case types.GET_TRIGGER_SOURCES_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          triggerSources: action.payload,
          isTriggerResourceFetch: false
        };
      case types.GET_TRIGGER_SOURCES_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isTriggerResourceFetch: true
        };
        case types.UPDATE_TRIGGER_SOURCE_STARTED:
          return {
            ...state,
            loading: true
          };
        case types.UPDATE_TRIGGER_SOURCE_SUCCEEDED:
          return {
            ...state,
            loading: false,
            error: null,
            updatedData: action.payload
          };
        case types.UPDATE_TRIGGER_SOURCE_FAILED:
          return {
            ...state,
            loading: false,
            error: action.payload
          };
      default:
          return{
              ...state
          };
        }
    }