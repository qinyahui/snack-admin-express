import * as types from '../lib/ActionTypes';

const initialState = {
  loading: false,
  FAQ: [],
  error: null,
  faqs: [],
  categories: [],
  isFAQFetch: null,
  isFAQCreated: null,
  isFAQUpdated: null
};

export default function FAQReducer(state, action) {
  if(typeof state === "undefined"){
    return initialState;
  }
  switch (action.type) {
    case types.GET_FAQ_STARTED:
      return {
        ...state,
        loading: true,
        isFAQFetch: false
      };
    case types.GET_FAQ_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        FAQ: action.payload,
        isFAQFetch: true
      };
    case types.GET_FAQ_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isFAQFetch: false
      };
    case types.CREATE_FAQ_STARTED:
      return {
        ...state,
        loading: true,
        isFAQCreated: false
      };
    case types.CREATE_FAQ_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        faqs: action.payload,
        isFAQCreated: true
      };
    case types.CREATE_FAQ_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isFAQCreated: false
      };
    case types.UPDATE_FAQ_STARTED:
      return {
        ...state,
        loading: true,
        isFAQUpdated: false
      };
    case types.UPDATE_FAQ_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        faqs: action.payload,
        isFAQUpdated: true
      };
    case types.UPDATE_FAQ_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
        isFAQUpdated: false
      };
    case types.GET_FAQ_CATEGORIES_STARTED:
      return {
        ...state,
        loading: true
      };
    case types.GET_FAQ_CATEGORIES_SUCCEEDED:
      return {
        ...state,
        loading: false,
        error: null,
        categories: action.payload
      };
    case types.GET_FAQ_CATEGORIES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    default:
      return {
        ...state
      };
  }
}