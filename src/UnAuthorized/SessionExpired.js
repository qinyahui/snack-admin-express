import React, { Component } from 'react';
import { Col, Container, Row, Button } from 'reactstrap';
import {checkAuthorization} from '../actions/Common';
import {connect} from 'react-redux';

class SessionExpire extends Component{
    login = () =>{
        this.props.checkAuthorization(localStorage.getItem('userId'));
    }
 
            render() {
                return (
                  <div className="app flex-row align-items-center">{
                      this.props.token ? window.location.reload():
                  
                    <Container>
                      <Row className="justify-content-center">
                        <Col md="6">
                          <span className="clearfix">
                            <h4 className="pt-3">Your session has been expired please login again.</h4>
                            <Button onClick={()=>this.login()} className="login">Login</Button>
                          </span>
                        </Col>
                      </Row>
                    </Container>}
                  </div>
                );
        
}
}
const mapStateToProps = state => {
    return {
        token: state.CommonReducer.token
    };
  };
  
  const mapDispatchToProps = {
  checkAuthorization
  }
  const SessionExpired = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SessionExpire);
  
  export default SessionExpired;