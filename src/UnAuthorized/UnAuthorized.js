import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';


export default class UnAuthorized extends Component{
 
            render() {
                return (
                  <div className="app flex-row align-items-center">
                    <Container>
                      <Row className="justify-content-center">
                        <Col md="6">
                          <span className="clearfix">
                            <h4 className="pt-3">You are not athourized to access this.</h4>
                            <p className="text-muted float-left">The page you are looking for is temporarily unavailable.</p>
                          </span>
                        </Col>
                      </Row>
                    </Container>
                  </div>
                );
        
}
}